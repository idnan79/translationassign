import { useState } from "react";
import { useSelector } from "react-redux";
import { Redirect, Link } from "react-router-dom";
import AppContainer from "../../hoc/AppContainer";

const Register = () => {
  const { loggedIn } = useSelector((state) => state.sessionReducer);

  const [user, setUser] = useState({
    username: "",
  });

  const onRegisterSubmit = (event) => {
    event.preventDefault();
    console.log("Register.onRegisterSubmit()", user);
  };

  const onInputChange = (event) => {
    setUser({
      ...user,
      [event.target.id]: event.target.value,
    });
  };

  return (
    <AppContainer>
      {loggedIn && <Redirect to="/Translatepage" />}
      <form className="mb-3" onSubmit={onRegisterSubmit}>
        <h1>Register To Login</h1>
        <p>Enter your name to register</p>

        <div className="mb-3">
          <label htmlFor="username" className="form-label">
            Chosse a Username
          </label>
          <input
            onChange={onInputChange}
            type="text"
            id="username"
            className="form-control"
            placeholder="Idnan"
          />
        </div>
        <button className="btn btn-success btn-lg">Register</button>
      </form>
      <p className="mv-3">
        <Link to="/">Already register? Login here</Link>
      </p>
    </AppContainer>
  );
};

export default Register;
