import AppContainer from "../../hoc/AppContainer";
import { useHistory } from "react-router-dom";
import ProfileView from "../Profile/ProfileView";

const Profile = () => {
  const history = useHistory();

  const getDataFromStorage = () => {
    let storedTranslations = JSON.parse(localStorage.getItem("translations"));
    return storedTranslations;
  };

  const backToHome = () => {
    history.push("/");
  };

  const logOut = () => {
    localStorage.clear();
    history.push("/");
  };

  return (
    <AppContainer>
      <div>
        <div>
          <h1>Profile</h1>
          <button type="button" class="btn mr-3" onClick={backToHome}>
            Home
          </button>
          <button type="button" class="btn mr-3" onClick={logOut}>
            Log Out
          </button>
          <br />
          <br></br>
        </div>
        <div>
          <h5>Your have searched following translations: </h5>
          {getDataFromStorage().map((wordToPass) => (
            <ProfileView word={wordToPass} />
          ))}
        </div>
      </div>
    </AppContainer>
  );
};
