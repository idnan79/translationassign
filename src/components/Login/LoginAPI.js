export const LoginAPI = {
  login(credentials) {
    return fetch(
      `http://localhost:8000/users/?username=${credentials.username}`
    )
      .then(async (response) => {
        if (!response.ok) {
          const { error = " an known error" } = await response.json();
          throw new Error(error);
        }
        return response.json();
      })
      .then((users) => {
        if (users.length === 0) {
          throw new Error("the user does not exist");
        }
        return users.pop();
      });
  },
};
